import fa from "element-ui/src/locale/lang/fa";

export default {
    namespaced: true,
    state: {
        gameState:1,
        WagerString:'',// 过关投注WagerString字段,
        betdata:[],  //当前下注订单列表
        CadID: sessionStorage.getItem("CadID") || 1,  //当前球类id
        Historydata:[],
        HistorySettled:[],
    },
    getters: {
        getCadID:(state) =>{
          return state.CadID
        },
        getgameState:(state) =>{
          return state.gameState
        },
        getHistorydata:(state) =>{
          return state.Historydata
        },
        getHistorySettled:(state) =>{
          return state.HistorySettled
        },
        getBetdata:(state) => {

            console.log(state.betdata);
            let e = state.betdata
            for (var i=0;i<e.length;i++) {
                e[i].Amount = ''
                e[i].BetMax = 100000
                e[i].BetMin = 10
                if(e[i].WagerTypeID === 101 || e[i].WagerTypeID === 103){
                    if(e[i].type === 0){
                        e[i].bet_title = e[i].HomeTeamStr
                        e[i].Cutline  =  e[i].Odds.HomeHdp===''?'+'+e[i].Odds.AwayHdp:'-'+e[i].Odds.HomeHdp
                        e[i].OddsValue = e[i].Odds.HomeHdpOdds
                    }else if(state.betdata[i].type === 1){
                        e[i].Cutline  =  e[i].Odds.AwayHdp===''?'+'+e[i].Odds.HomeHdp:'-'+e[i].Odds.AwayHdp
                        e[i].bet_title = e[i].AwayTeamStr
                        e[i].OddsValue = e[i].Odds.AwayHdpOdds
                    }else {

                    }
                }
                if(e[i].WagerTypeID === 102 || e[i].WagerTypeID ===104 || e[i].WagerTypeID ===109){
                    if(e[i].type === 0){
                        e[i].bet_title = '大'
                        e[i].Cutline  = e[i].Odds.OULine
                        e[i].OddsValue = e[i].Odds.OverOdds
                        e[i].Amount = ''
                    }else if(state.betdata[i].type === 1){
                        e[i].bet_title = '小'
                        e[i].Cutline  = e[i].Odds.OULine
                        e[i].OddsValue = e[i].Odds.UnderOdds
                    }else {

                    }
                }
                if(e[i].WagerTypeID === 111){
                    if(e[i].type === 0){
                        e[i].bet_title = e[i].HomeTeamStr
                        e[i].Cutline  = 0
                        e[i].OddsValue = e[i].Odds.HomeOdds
                        e[i].Amount = ''
                    }else if(state.betdata[i].type === 1){
                        e[i].bet_title = e[i].AwayTeamStr
                        e[i].Cutline  = 0
                        e[i].OddsValue = e[i].Odds.AwayOdds
                        e[i].Amount = ''
                    }else {

                    }
                }


                   var ids = e.map(value => value.EvtID)
                   ids.splice(i,1)
                    if(ids.indexOf(e[i].EvtID)>-1){
                        e[i].isrepeat = true
                    }else {
                        e[i].isrepeat = false
                    }


                // console.log(ids);
                // var idsSet = new Set(ids);
                // console.log(idsSet);
                // if(idsSet.size == ids.length){
                //         e[i].isrepeat = false
                //         console.log('不存在同个值')
                //     }else{
                //         e[i].isrepeat = true
                //         console.log('“存在同个值”')
                //
                //     }

                // if(e.every(target=>target.EvtID !== e[i].EvtID)){
                //     console.log('没得')
                // }else {
                //     console.log('阔以')
                // }
                // for (var j = i + 1; j < e.length; j++){
                //     if(e[i].EvtID === e[j].EvtID){
                //         e[i].isrepeat = true
                //     }
                // }

            }
            return e
        },
        getBestHead:(state) => {
            return state.BestHead
        },
        getBestList:(state) => {
            console.log(state.BestList);
            return state.BestList
        },
        WagerString:(state) => {
            return state.WagerString
        },


    },
    mutations: {
        ADD_BERDATA(state,data){

            console.log(state.betdata);
            if(state.betdata.length<=0){
                state.betdata.push(data)
                return
            }
            for (var i = 0 ;i<state.betdata.length;i++) {
                if(state.betdata[i].Odds.GameID === data.Odds.GameID){
                    state.betdata.splice(i,1,data)
                    return;
                }
            }
            state.betdata.push(data)

        },
        DELETED_BERDATA(state,i){
            if(i === 'all'){
                state.betdata = []
            }else {
                state.betdata.splice(i,1)
            }

        },

        ADD_WAGERSTRING(state,val){
            if(state.WagerString === ''){
                state.WagerString += val
            }else {
                state.WagerString += ('|'+val)
            }
        },

        SET_CADID(state,val){
            console.log(val);

            state.CadID = val?val:1
        },
        SET_TABLEDATA(state,data){
            state.BestHead =  data.BestHead
            state.BestList =  data.List
        },
        SET_HISTORY(state,data){
            state.Historydata = data
        },
        SET_HISTORYSETTLED(state,data){
             state.HistorySettled= data
        },
        SET_GAMESTATE(state,data){
             state.gameState= data
        },

    },
    actions: {

    }
}
